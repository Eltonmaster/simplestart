curl https://flathub.org/repo/appstream/com.spotify.Client.flatpakref -o spotify.flatpakref
curl https://flathub.org/repo/appstream/org.gimp.GIMP.flatpakref -o gimp.flatpakref
curl https://flathub.org/repo/appstream/org.videolan.VLC.flatpakref -o vlc.flatpakref
curl https://flathub.org/repo/appstream/com.valvesoftware.Steam.flatpakref -o steam.flatpakref
curl https://flathub.org/repo/appstream/com.obsproject.Studio.flatpakref -o obs.flatpakref
curl https://flathub.org/repo/appstream/com.discordapp.Discord.flatpakref -o discord.flatpakref
curl https://flathub.org/repo/appstream/com.visualstudio.code.flatpakref -o code.flatpakref
curl https://flathub.org/repo/appstream/org.gabmus.hydrapaper.flatpakref -o hydra.flatpakref
flatpak install spotify.flatpakref -y
flatpak install gimp.flatpakref -y
flatpak install vlc.flatpakref -y
flatpak install steam.flatpakref -y
flatpak install obs.flatpakref -y
flatpak install discord.flatpakref -y
flatpak install code.flatpakref -y
flatpak install hydra.flatpakref -y
rm *flatpakref
