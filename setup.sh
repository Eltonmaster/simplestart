sudo apt update
sudo apt upgrade
sudo apt install -y snapd ffmpeg python3 audacity build-essential gnome-tweak-tool network-manager-vpnc-gnome openssh-client rclone sqlitebrowser htop guake flatpak
flatpak remote-add --user --if-not-exists flathub https://flathub.org/repo/flathub.flatpakrepo
sudo snap install mailspring
sudo snap install postman
sudo snap install firefox
sudo snap install chromium
sudo snap install youtube-dl
sudo snap install code --classic
sudo apt update
sudo apt install apt-transport-https ca-certificates curl gnupg-agent software-properties-common
curl -fsSL https://download.docker.com/linux/ubuntu/gpg | sudo apt-key add - && sudo apt-key fingerprint 0EBFCD88
sudo add-apt-repository "deb [arch=amd64] https://download.docker.com/linux/ubuntu $(lsb_release -cs) stable"
sudo apt-get update && sudo apt-get install docker-ce docker-ce-cli containerd.io
mkdir ~/.scripts
mv ./scripts/* ~/.scripts
echo 'export PATH="$HOME/.scripts:$PATH"' >> ~/.bashrc
echo 'alias upgrade="sudo apt update && sudo apt upgrade -y && sudo flatpak update -y && sudo snap refresh"' >> ~./bashrc
